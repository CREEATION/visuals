package com.zeitheron.visuals.blocks;

import java.util.Map;
import java.util.UUID;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import com.mojang.authlib.minecraft.MinecraftProfileTexture.Type;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.visuals.Visuals;
import com.zeitheron.visuals.client.particle.FXDiggingTexture;

import net.minecraft.block.BlockSkull;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.ParticleManager;
import net.minecraft.client.resources.DefaultPlayerSkin;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.tileentity.TileEntitySkull;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockSkullFixed extends BlockSkull
{
	private static final ResourceLocation SKELETON_TEXTURES = new ResourceLocation("textures/entity/skeleton/skeleton.png");
	private static final ResourceLocation WITHER_SKELETON_TEXTURES = new ResourceLocation("textures/entity/skeleton/wither_skeleton.png");
	private static final ResourceLocation ZOMBIE_TEXTURES = new ResourceLocation("textures/entity/zombie/zombie.png");
	private static final ResourceLocation CREEPER_TEXTURES = new ResourceLocation("textures/entity/creeper/creeper.png");
	private static final ResourceLocation DRAGON_TEXTURES = new ResourceLocation("textures/entity/enderdragon/dragon.png");
	
	{
		setHardness(1.0F);
		setSoundType(SoundType.STONE);
		setTranslationKey("skull");
	}
	
	@SideOnly(Side.CLIENT)
	public static ResourceLocation getSkullTexture(TileEntitySkull skull)
	{
		switch(skull.getSkullType())
		{
		case 0:
		default:
			return Visuals.proxy.trimToHead(SKELETON_TEXTURES);
		case 1:
			return Visuals.proxy.trimToHead(WITHER_SKELETON_TEXTURES);
		case 2:
			return Visuals.proxy.trimToHead(ZOMBIE_TEXTURES);
		case 3:
			ResourceLocation resourcelocation = DefaultPlayerSkin.getDefaultSkinLegacy();
			
			GameProfile profile = skull.getPlayerProfile();
			
			if(profile != null)
			{
				Minecraft minecraft = Minecraft.getMinecraft();
				Map<Type, MinecraftProfileTexture> map = minecraft.getSkinManager().loadSkinFromCache(profile);
				
				if(map.containsKey(Type.SKIN))
				{
					resourcelocation = minecraft.getSkinManager().loadSkin(map.get(Type.SKIN), Type.SKIN);
				} else
				{
					UUID uuid = EntityPlayer.getUUID(profile);
					resourcelocation = DefaultPlayerSkin.getDefaultSkin(uuid);
				}
			}
			
			return Visuals.proxy.trimToHead(resourcelocation);
		case 4:
			return Visuals.proxy.trimToHead(CREEPER_TEXTURES);
		case 5:
			return Visuals.proxy.trimToHead(DRAGON_TEXTURES);
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean addDestroyEffects(World world, BlockPos pos, ParticleManager manager)
	{
		TileEntitySkull skull = WorldUtil.cast(world.getTileEntity(pos), TileEntitySkull.class);
		if(skull != null)
			for(int j = 0; j < 4; ++j)
			{
				for(int k = 0; k < 4; ++k)
				{
					for(int l = 0; l < 4; ++l)
					{
						double d0 = ((double) j + 0.5D) / 4.0D;
						double d1 = ((double) k + 0.5D) / 4.0D;
						double d2 = ((double) l + 0.5D) / 4.0D;
						manager.addEffect(new FXDiggingTexture(world, (double) pos.getX() + d0, (double) pos.getY() + d1, (double) pos.getZ() + d2, d0 - 0.5D, d1 - 0.5D, d2 - 0.5D, getSkullTexture(skull)).setBlockPos(pos));
					}
				}
			}
		
		return skull != null;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean addHitEffects(IBlockState state, World world, RayTraceResult target, ParticleManager manager)
	{
		if(target == null || target.sideHit == null || target.getBlockPos() == null)
			return false;
		
		BlockPos pos = target.getBlockPos();
		EnumFacing side = target.sideHit;
		
		TileEntitySkull skull = WorldUtil.cast(world.getTileEntity(pos), TileEntitySkull.class);
		if(skull == null)
			return false;
		
		int i = pos.getX();
		int j = pos.getY();
		int k = pos.getZ();
		float f = 0.1F;
		AxisAlignedBB axisalignedbb = state.getBoundingBox(world, pos);
		double d0 = (double) i + world.rand.nextDouble() * (axisalignedbb.maxX - axisalignedbb.minX - 0.20000000298023224D) + 0.10000000149011612D + axisalignedbb.minX;
		double d1 = (double) j + world.rand.nextDouble() * (axisalignedbb.maxY - axisalignedbb.minY - 0.20000000298023224D) + 0.10000000149011612D + axisalignedbb.minY;
		double d2 = (double) k + world.rand.nextDouble() * (axisalignedbb.maxZ - axisalignedbb.minZ - 0.20000000298023224D) + 0.10000000149011612D + axisalignedbb.minZ;
		
		if(side == EnumFacing.DOWN)
			d1 = (double) j + axisalignedbb.minY - 0.10000000149011612D;
		if(side == EnumFacing.UP)
			d1 = (double) j + axisalignedbb.maxY + 0.10000000149011612D;
		if(side == EnumFacing.NORTH)
			d2 = (double) k + axisalignedbb.minZ - 0.10000000149011612D;
		if(side == EnumFacing.SOUTH)
			d2 = (double) k + axisalignedbb.maxZ + 0.10000000149011612D;
		if(side == EnumFacing.WEST)
			d0 = (double) i + axisalignedbb.minX - 0.10000000149011612D;
		if(side == EnumFacing.EAST)
			d0 = (double) i + axisalignedbb.maxX + 0.10000000149011612D;
		
		manager.addEffect(new FXDiggingTexture(world, d0, d1, d2, 0.0D, 0.0D, 0.0D, getSkullTexture(skull)).setBlockPos(pos).multiplyVelocity(0.2F).multipleParticleScaleBy(0.6F));
		
		return true;
	}
	
	@Override
	public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player)
	{
		ItemStack item = super.getPickBlock(state, target, world, pos, player);
		TileEntitySkull skull = WorldUtil.cast(world.getTileEntity(pos), TileEntitySkull.class);
		if(skull != null)
		{
			item = new ItemStack(Items.SKULL, 1, skull.getSkullType());
			if(skull.getSkullType() == 3 && skull.getPlayerProfile() != null)
			{
				item.setTagCompound(new NBTTagCompound());
				NBTTagCompound nbt = new NBTTagCompound();
				NBTUtil.writeGameProfile(nbt, skull.getPlayerProfile());
				item.getTagCompound().setTag("SkullOwner", nbt);
			}
		}
		return item;
	}
}