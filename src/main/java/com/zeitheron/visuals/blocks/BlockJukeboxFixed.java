package com.zeitheron.visuals.blocks;

import com.zeitheron.visuals.tiles.TileEntityJukeboxFixed;

import net.minecraft.block.BlockJukebox;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockJukeboxFixed extends BlockJukebox
{
	{
		setHardness(2.0F);
		setResistance(10.0F);
		setSoundType(SoundType.WOOD);
		setTranslationKey("jukebox");
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean canHarvestBlock(IBlockAccess world, BlockPos pos, EntityPlayer player)
	{
		return true;
	}
	
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileEntityJukeboxFixed();
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		if(((Boolean) state.getValue(HAS_RECORD)).booleanValue())
		{
			this.dropRecord2(worldIn, pos, state);
			state = state.withProperty(HAS_RECORD, Boolean.valueOf(false));
			worldIn.setBlockState(pos, state, 2);
			return true;
		} else
		{
			return false;
		}
	}
	
	@Override
	public void insertRecord(World worldIn, BlockPos pos, IBlockState state, ItemStack recordStack)
	{
		TileEntity tileentity = worldIn.getTileEntity(pos);
		
		if(tileentity instanceof TileEntityJukeboxFixed)
		{
			TileEntityJukeboxFixed te = (TileEntityJukeboxFixed) tileentity;
			te.setRecord(recordStack.copy());
			worldIn.setBlockState(pos, state.withProperty(HAS_RECORD, Boolean.valueOf(true)), 2);
		}
	}
	
	private void dropRecord2(World worldIn, BlockPos pos, IBlockState state)
	{
		if(!worldIn.isRemote)
		{
			TileEntity tileentity = worldIn.getTileEntity(pos);
			
			if(tileentity instanceof TileEntityJukeboxFixed)
			{
				TileEntityJukeboxFixed te = (TileEntityJukeboxFixed) tileentity;
				ItemStack itemstack = te.getRecord();
				
				if(!itemstack.isEmpty())
				{
					worldIn.playEvent(1010, pos, 0);
					worldIn.playRecord(pos, null);
					te.setRecord(ItemStack.EMPTY);
					float f = 0.7F;
					double d0 = (double) (worldIn.rand.nextFloat() * 0.7F) + 0.15000000596046448D;
					double d1 = (double) (worldIn.rand.nextFloat() * 0.7F) + 0.06000000238418579D + 0.6D;
					double d2 = (double) (worldIn.rand.nextFloat() * 0.7F) + 0.15000000596046448D;
					te.dropStack = itemstack.copy();
				}
			}
		}
	}
}