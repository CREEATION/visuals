package com.zeitheron.visuals.api.client;

import com.zeitheron.hammercore.lib.zlib.utils.IndexedMap;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class WLAR<T extends TileEntity>
{
	private static final IndexedMap<Class<? extends TileEntity>, WLAR> tileWLARs = new IndexedMap();
	private static final IndexedMap<Block, WLAR> blockWLARs = new IndexedMap();
	
	public static <T extends TileEntity> void register(Class<T> type, WLAR<T> render)
	{
		tileWLARs.put(type, render);
	}
	
	public static void register(Block type, WLAR render)
	{
		blockWLARs.put(type, render);
	}
	
	public static void render(World world, TileEntity tile, IBlockState state, BlockPos pos, double x, double y, double z, float partialTime, NonNullList<ItemStack> items)
	{
		WLAR w;
		if(tile != null)
		{
			w = tileWLARs.get(tile.getClass());
			if(w != null)
			{
				w.render(tile, x, y, z, partialTime, items);
				return;
			}
		}
		w = blockWLARs.get(state.getBlock());
		if(w != null)
		{
			w.render(world, pos, state, x, y, z, partialTime, items);
			return;
		}
	}
	
	public void render(T tile, double x, double y, double z, float partialTime, NonNullList<ItemStack> items)
	{
	}
	
	public void render(World world, BlockPos pos, IBlockState state, double x, double y, double z, float partialTime, NonNullList<ItemStack> items)
	{
	}
}