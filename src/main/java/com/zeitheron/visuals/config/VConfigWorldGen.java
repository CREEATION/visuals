package com.zeitheron.visuals.config;

import com.zeitheron.hammercore.cfg.HCModConfigurations;
import com.zeitheron.hammercore.cfg.IConfigReloadListener;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyBool;

@HCModConfigurations(modid = "visuals", isModule = true, module = "worldgen")
public class VConfigWorldGen implements IConfigReloadListener
{
	@ModConfigPropertyBool(category = "fixes", name = "Underground Lilypads", comment = "Do we fix lilypads that may spawn in caves? (Removes lilypads completely)", defaultValue = true)
	public static boolean fix_lilypads;
	
	@ModConfigPropertyBool(category = "fixes", name = "Falling Blocks", comment = "Do we fix fallable blocks? (Replaces lowest fallable with solid alternative, if possible)", defaultValue = true)
	public static boolean fix_falling;
	
	@ModConfigPropertyBool(category = "features", name = "Organic Leftovers", comment = "Replace plants on dirt in caves to organic leftovers?", defaultValue = true)
	public static boolean feature_organicLeftovers;
}