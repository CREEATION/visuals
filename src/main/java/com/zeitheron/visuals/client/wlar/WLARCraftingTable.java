package com.zeitheron.visuals.client.wlar;

import com.zeitheron.visuals.api.client.WLAR;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public class WLARCraftingTable extends WLAR<TileEntity>
{
	@Override
	public void render(World world, BlockPos pos, IBlockState state, double x, double y, double z, float partialTime, NonNullList<ItemStack> items)
	{
		if(items.size() != 10)
			return;
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(x + 5 / 16F, y + 1, z + 5 / 16F);
		GlStateManager.scale(1 / 8F, 1 / 8F, 1 / 8F);
		
		RenderItem render = Minecraft.getMinecraft().getRenderItem();
		
		for(int i = 0; i < 9; ++i)
		{
			ItemStack item = items.get(i);
			
			float xc = (2 - i % 3) / 3F;
			float zc = (2 - i / 3) / 3F;
			
			GlStateManager.pushMatrix();
			GlStateManager.translate(xc * 4.5, 0, zc * 4.5);
			GlStateManager.rotate(90, 1, 0, 0);
			render.renderItem(item, TransformType.FIXED);
			GlStateManager.popMatrix();
			
			GlStateManager.color(1F, 1F, 1F, 1F);
			GlStateManager.disableLighting();
		}
		
		ItemStack output = items.get(9);
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(4.5 / 3F, 2F + MathHelper.sin((float) Math.toRadians((world.getTotalWorldTime() * 2) % 360F + 2 * partialTime)) * .25F, 4.5 / 3F);
		GlStateManager.scale(1.5, 1.5, 1.5);
		render.renderItem(output, TransformType.FIXED);
		GlStateManager.popMatrix();
		
		GlStateManager.color(1F, 1F, 1F, 1F);
		GlStateManager.disableLighting();
		
		GlStateManager.popMatrix();
	}
}