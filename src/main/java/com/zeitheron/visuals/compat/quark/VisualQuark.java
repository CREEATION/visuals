package com.zeitheron.visuals.compat.quark;

import com.zeitheron.hammercore.mod.ModuleLoader;
import com.zeitheron.visuals.compat.base.VisualsCompat;

@ModuleLoader(requiredModid = "quark")
public class VisualQuark extends VisualsCompat
{
	@Override
	public void init()
	{
		((VQS) getProxy()).init();
	}
	
	@Override
	public String getClientProxyClass()
	{
		return "com.zeitheron.visuals.compat.quark.VQC";
	}

	@Override
	public String getServerProxyClass()
	{
		return "com.zeitheron.visuals.compat.quark.VQS";
	}
}